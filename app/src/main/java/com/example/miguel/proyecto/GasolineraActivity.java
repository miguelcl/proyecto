package com.example.miguel.proyecto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class GasolineraActivity extends AppCompatActivity {
    private Button datos;
    private Button productos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gasolinera);
        datos = (Button)findViewById(R.id.btnDato);
        productos=(Button)findViewById(R.id.btnProducto);

        productos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(GasolineraActivity.this, Productos.class);
                startActivity(i);
            }
        });

    }
}
